import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import classNames from 'classnames';

import Spinner from '../layout/Spinner';

class ClientDetails extends Component {
  state = {
    showBalanceUpdate: false,
    balanceUpdateAmount: ''
  };

  handleChange = (event) =>
    this.setState({ [event.target.name]: event.target.value });

  handleSubmit = (event) => {
    event.preventDefault();
    const { client, firestore } = this.props;
    const { balanceUpdateAmount } = this.state;

    const clientUpdate = {
      balance: parseFloat(balanceUpdateAmount)
    };

    firestore.update({ collection: 'clients', doc: client.id }, clientUpdate);
  };

  handleDelete = async () => {
    const { client, firestore, history } = this.props;
    await firestore.delete({ collection: 'clients', doc: client.id });
    history.push('/');
  };

  render() {
    const { client } = this.props;
    const { showBalanceUpdate, balanceUpdateAmount } = this.state;

    const balanceForm = (
        <form onSubmit={this.handleSubmit}>
          <div className="input-group">
            <input
              name="balanceUpdateAmount"
              placeholder="Add new balance"
              type="text"
              className="form-control"
              onChange={this.handleChange}
              value={balanceUpdateAmount}
            />
            <div className="input-gorup-append">
              <input
                type="submit"
                value="Update"
                className="btn btn-outline-dark"
              />
            </div>
          </div>
        </form>
      );

    return client ? (
      <div>
        <div className="row">
          <div className="col-md-6">
            <Link to="/" className="btn btn-link">
              <i className="fas fa-arrow-circle-left" /> Back To Dashboard
            </Link>
          </div>
          <div className="col-md-6">
            <div className="btn-group fliat-right">
              <Link to={`/client/edit/${client.id}`} className="btn btn-dark">
                Edit
              </Link>
            </div>
            <button className="btn btn-danger" onClick={this.handleDelete}>
              Delete
            </button>
          </div>
        </div>
        <hr />
        <div className="card">
          <h3 className="card-header">
            {client.firstName} {client.lastName}
          </h3>
          <div className="card-body">
            <div className="row">
              <div className="col-md-8 col-sm-6">
                <h4>
                  Client ID: <span className="text-secondary">{client.id}</span>
                </h4>
              </div>
              <div className="col-md-4 col-sm-6">
                <h3 className="pull-right">
                  Balance:
                  <span
                    className={classNames({
                      'text-danger': client.balance,
                      'text-success': !client.balance
                    })}
                  >
                    ${parseFloat(client.balance).toFixed(2)}
                  </span>
                  <small>
                    <a
                      href="#!"
                      onClick={() =>
                        this.setState({ showBalanceUpdate: !showBalanceUpdate })
                      }
                    >
                      <i className="fas fa-pencil-alt" />
                    </a>
                  </small>
                </h3>
                {showBalanceUpdate && balanceForm}
              </div>
            </div>
            <hr />
            <ul className="list-group">
              <li className="list-group-item">Phone: {client.phone}</li>
              <li className="list-group-item">Email: {client.email}</li>
            </ul>
          </div>
        </div>
      </div>
    ) : (
      <Spinner />
    );
  }
}

export default compose(
  firestoreConnect(({ match }) => [
    { collection: 'clients', storeAs: 'client', doc: match.params.id }
  ]),
  connect(({ firestore: { ordered } }, props) => ({
    client: ordered.client && ordered.client[0]
  }))
)(ClientDetails);
