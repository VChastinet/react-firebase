import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';

import Spinner from '../layout/Spinner';

class Clients extends Component {
  state = {
    totalOwned: 5
  };

  static getDerivedStateFromProps(props) {
    const { clients } = props;

    if (clients) {
      const totalOwned = clients.reduce((total, client) => +client.balance + total, 0);

      return { totalOwned }
    }

    return null;
  }

  render() {
    const { clients } = this.props;
    const { totalOwned } = this.state;

    return clients ? (
      <div>
        <div className="row">
          <div className="col-md-6">
            <h2>
              <i className="fas fa-users" /> Clients
            </h2>
          </div>
          <div className="col-md-6">
            <h5 className="text-right text-secondary">
              Total Owned <span className="text-primary">${totalOwned.toFixed(2)}</span>
            </h5>
          </div>
        </div>
        <table className="table table-striped">
          <thead className="thead-inverse">
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Balance</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {clients.map(({ id, email, firstName, lastName, balance }) => (
              <tr key={id}>
                <td>
                  {firstName} {lastName}
                </td>
                <td>{email}</td>
                <td>${parseFloat(balance).toFixed(2)}</td>
                <td>
                  <Link
                    to={`/client/${id}`}
                    className="btn btn-secondary btn-sm"
                  >
                    <i className="fas fa-arrow-circle-right" /> Details
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    ) : (
      <Spinner />
    );
  }
}

export default compose(
  firestoreConnect([{ collection: 'clients' }]),
  connect((state, props) => ({
    clients: state.firestore.ordered.clients
  }))
)(Clients);
