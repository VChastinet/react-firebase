import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import Spinner from '../layout/Spinner';

class EditClient extends Component {
  constructor(props) {
    super(props);
    this.firstNameInput = React.createRef();
    this.lastNameInput = React.createRef();
    this.emailInput = React.createRef();
    this.phoneInput = React.createRef();
    this.balanceInput = React.createRef();

    this.state = {
      loading: false
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const { client, firestore, history } = this.props;
    const currBalance = this.balanceInput.current.value;
    const updatedClient = {
      firstName: this.firstNameInput.current.value,
      lastName: this.lastNameInput.current.value,
      email: this.emailInput.current.value,
      phone: this.phoneInput.current.value,
      balance: currBalance === '' ? 0 : currBalance
    };

    this.setState({ loading: true });

    await firestore.update(
      { collection: 'clients', doc: client.id },
      updatedClient
    );

    history.push('/');
  };
  render() {
    const {
      client,
      settings: { disableBalanceOnEdit }
    } = this.props;
    const { loading } = this.state;
    return client ? (
      <div>
        <h1>Edit Client</h1>
        <div className="row">
          <div className="col-md-6">
            <Link to="/" className="btn btn-link">
              <i className="fas fa-arrow-circle-left" /> Back to Dashboard
            </Link>
          </div>
        </div>
        <div className="card">
          <div className="card-header">Add Client</div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <input
                  type="text"
                  className="form-control"
                  name="firstName"
                  minLength="2"
                  required
                  ref={this.firstNameInput}
                  defaultValue={client.firstName}
                />
              </div>
              <div className="form-group">
                <label htmlFor="lastName">Last Name</label>
                <input
                  type="text"
                  className="form-control"
                  name="lastName"
                  minLength="2"
                  required
                  ref={this.lastNameInput}
                  defaultValue={client.lastName}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  ref={this.emailInput}
                  defaultValue={client.email}
                />
              </div>

              <div className="form-group">
                <label htmlFor="phone">Phone</label>
                <input
                  type="text"
                  className="form-control"
                  name="phone"
                  minLength="10"
                  required
                  ref={this.phoneInput}
                  defaultValue={client.phone}
                />
              </div>

              <div className="form-group">
                <label htmlFor="balance">Balance</label>
                <input
                  type="text"
                  className="form-control"
                  name="balance"
                  ref={this.balanceInput}
                  defaultValue={client.balance}
                  disabled={disableBalanceOnEdit}
                />
              </div>
              <input
                type="submit"
                value={loading ? 'saving...' : 'submit'}
                className="btn btn-primary btn-block"
              />
            </form>
          </div>
        </div>
      </div>
    ) : (
      <Spinner />
    );
  }
}

export default compose(
  firestoreConnect(({ match }) => [
    { collection: 'clients', storeAs: 'client', doc: match.params.id }
  ]),
  connect(({ firestore: { ordered }, settings }) => ({
    client: ordered.client && ordered.client[0],
    settings
  }))
)(EditClient);
