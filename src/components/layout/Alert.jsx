import React from 'react';
import classNames from 'classnames';

const Alert = ({ message, messageType }) => {
  const alertTypes = {
    'alert-success': messageType === 'success',
    'alert-danger': messageType === 'error'
  };
  return <div className={classNames('alert', alertTypes)}>{message}</div>;
};

export default Alert;
