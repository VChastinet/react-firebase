import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import { notifyUser } from '../../actions/notifyAction';
import Alert from '../layout/Alert';

class Login extends Component {
  state = {
    email: '',
    password: ''
  };

  handleChange = ({ target }) => this.setState({ [target.name]: target.value });

  handleSubmit = async (event) => {
    event.preventDefault();

    const { firebase, notifyUser } = this.props;
    const { email, password } = this.state;
    try {
      await firebase.login({
        email,
        password
      });
    } catch (error) {
      console.warn('error: ', error);
      notifyUser('Invalid Login Credentials', 'error');
    }
  };

  render() {
    const {
      notify: { message, messageType }
    } = this.props;
    const { email, password } = this.state;
    return (
      <div className="row">
        <div className="col-md-6 mx-auto">
          <div className="card">
            <div className="card-body">
              {message && <Alert message={message} messageType={messageType} />}
              <h1 className="text-center pb-4 pt-3">
                <span className="text-primary">
                  <i className="fas fa-lock" /> Login
                </span>
              </h1>
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input
                    onChange={this.handleChange}
                    name="email"
                    required
                    value={email}
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input
                    onChange={this.handleChange}
                    name="password"
                    required
                    value={password}
                    type="password"
                    className="form-control"
                  />
                </div>
                <input
                  value="Login"
                  type="submit"
                  className="btn btn-primary btn-block"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  firebaseConnect(),
  connect(
    (state) => ({
      notify: state.notify
    }),
    { notifyUser }
  ),
)(Login);
