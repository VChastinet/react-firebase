import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import { notifyUser } from '../../actions/notifyAction';
import Alert from '../layout/Alert';

class Register extends Component {
  state = {
    email: '',
    password: ''
  };

  componentWillMount() {
      const {
        settings: {
          allowRegistration,
        },
        history,
      } = this.props;

      if (!allowRegistration) {
        history.push('/');
      }
  }

  handleChange = ({ target }) => this.setState({ [target.name]: target.value });

  handleSubmit = async (event) => {
    event.preventDefault();

    const { firebase, notifyUser } = this.props;
    const { email, password } = this.state;

    try {
      await firebase.createUser({ email, password });
    } catch (error) {
      notifyUser('That User Already Exist', 'error');
    }

  };

  render() {
    const {
      notify: { message, messageType }
    } = this.props;
    const { email, password } = this.state;
    return (
      <div className="row">
        <div className="col-md-6 mx-auto">
          <div className="card">
            <div className="card-body">
              {message && <Alert message={message} messageType={messageType} />}
              <h1 className="text-center pb-4 pt-3">
                <span className="text-primary">
                  <i className="fas fa-lock" /> Register New User
                </span>
              </h1>
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input
                    onChange={this.handleChange}
                    name="email"
                    required
                    value={email}
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input
                    onChange={this.handleChange}
                    name="password"
                    required
                    value={password}
                    type="password"
                    className="form-control"
                  />
                </div>
                <input
                  value="Sign up"
                  type="submit"
                  className="btn btn-primary btn-block"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  firebaseConnect(),
  connect(
    ({ notify, settings }) => ({
      notify,
      settings
    }),
    { notifyUser }
  ),
)(Register);
