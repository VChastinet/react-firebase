import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  setAllowRegistration,
  setDisableBalanceOnAdd,
  setDisableBalanceOnEdit
} from '../../actions/settingsAction';

class Settings extends Component {

  handleAllowRegistration = () => {
    const { setAllowRegistration } = this.props;
    setAllowRegistration();
  }
  handleDisableBalanceOnAdd = () => {
    const { setDisableBalanceOnAdd } = this.props;
    setDisableBalanceOnAdd();
  }
  handleDisableBalanceOnEdit = () => {
    const { setDisableBalanceOnEdit } = this.props;
    setDisableBalanceOnEdit();
  }
  render() {
    const {
      settings: {
        allowRegistration,
        disableBalanceOnAdd,
        disableBalanceOnEdit
      }
    } = this.props;

    return (
      <div>
        <div className="row">
          <div className="col-md-6">
            <Link to="/" className="btn btn-link">
              <i className="fas fa-arrow-circle-left" /> Back To Dashboard
            </Link>
          </div>
        </div>
        <div className="card">
          <div className="card-header">Edit Settings</div>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="allowRegistration">Allow Registration</label>{' '}
                <input
                  type="checkbox"
                  id="allowRegistration"
                  checked={!!allowRegistration}
                  onChange={this.handleAllowRegistration}
                />
              </div>

              <div className="form-group">
                <label htmlFor="disableBalanceOnAdd">Disable Balance On Add</label>{' '}
                <input
                  type="checkbox"
                  id="disableBalanceOnAdd"
                  checked={!!disableBalanceOnAdd}
                  onChange={this.handleDisableBalanceOnAdd}
                />
              </div>

              <div className="form-group">
                <label htmlFor="disableBalanceOnEdit">Disable Balance On Edit</label>{' '}
                <input
                  type="checkbox"
                  id="disableBalanceOnEdit"
                  checked={!!disableBalanceOnEdit}
                  onChange={this.handleDisableBalanceOnEdit}
                />
              </div>

            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  ({ firebase, settings }) => ({
    auth: firebase.auth,
    settings: settings
  }),
  {
    setAllowRegistration,
    setDisableBalanceOnAdd,
    setDisableBalanceOnEdit
  }
)(Settings);
